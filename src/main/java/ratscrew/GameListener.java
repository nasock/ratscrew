package ratscrew;

public interface GameListener {
	public void onPutCard(Ratscrew g, Player p, Card c);
	public void onPlayerDeckGrab(Ratscrew g, Player p);
	public void onPlayerHasNoCards(Ratscrew g, Player p);
	public void onChangePlayer(Ratscrew g, Player p);
	public void onComplete(Ratscrew g, Player p);
	public void onSlap(Ratscrew g, Player p,boolean success);
}
