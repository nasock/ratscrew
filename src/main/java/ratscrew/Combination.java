package ratscrew;

public abstract class Combination {
	public abstract boolean isPresent (Deck d);
}
