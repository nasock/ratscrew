package ratscrew;

public abstract class Event {
	public Player player;
	public long executionTime;
	
	public Event(Player p) {
		this(p, 0);
	}
	
	public Event(Player p, long delay) {
		player = p;
		executionTime = System.currentTimeMillis() + delay;
	}
	
	public abstract void accept(Ratscrew game);
	
	public String toString() {
		return getClass().getSimpleName();
	}
}
