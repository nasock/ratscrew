package ratscrew;

public class Difficulty {
	private double chance;
	private double slapCoeff;
	
	public Difficulty(double ch, double sc) {
		chance = ch;
		slapCoeff = sc;
	}
	
	public double getChance() {
		return chance;
	}
	
	public double getSlapCoeff() {
		return slapCoeff;
	}
}
