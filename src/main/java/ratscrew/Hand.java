package ratscrew;

import java.awt.Color;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Hand extends JPanel {
	private CardImage cardImage;
	private JLabel label;

	public Hand(Image im,  double scale, String str) {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		cardImage = new CardImage(im, scale);

		label = new JLabel(str);
		label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		label.setForeground(Color.BLACK);
		label.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));

		add(cardImage);
		add(label);
	}

	public JButton addButton(String buttonName) {
		JButton button = new JButton(buttonName);
		button.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		add(button);
		return button;
	}
	
	public void setText(String str) {
		label.setText(str);
		label.repaint();
	}

	public void setImage(Image im) {
		cardImage.setImage(im);
	}
}
