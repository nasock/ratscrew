package ratscrew;

public class Player {
	private int id;
	private String name;
	private Deck playerDeck;
	private boolean human;
	private boolean enabled = true;
	private BehaviorFactory behaviorFactory;
	private BehaviorFactory.Behavior behavior;
	protected boolean isSlapped = false;

	public Player(int _id, String n, Deck d, boolean h) {
		name = n;
		setDeck(d);
		id = _id;
		human = h;
		behaviorFactory = new BehaviorFactory();
	}
	
	public void enable() {
		enabled = true;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	protected void disable() {
		enabled = false;
	}

	public void setDeck(Deck d) {
		playerDeck = d;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}
	
	public boolean isHuman() {
		return human;
	}
	
	public void onChangeSettings(Settings set) {
		
	}

	public int getDeckSize() {
		return playerDeck.getSize();
	}
	
	public Card popCard() {
		return playerDeck.pop();
	}

	public Deck popDeck(int count) {
		return playerDeck.popDeck(count);
	}

	public void addDeck(Deck otherDeck) {
		if (otherDeck.isEmpty()) {
			return;
		}
		playerDeck.addDeckToBottom(otherDeck);
	}

	public boolean isEmpty() {
		return playerDeck.isEmpty();
	}

	public String toString() {
		return name + " (" + playerDeck.getSize() + ")";
	}
	
	public void setBehavior(BehaviorFactory.Behavior b) {
		behavior = b;
	}
	
	public void init(Ratscrew game) {
		setBehavior(behaviorFactory.getNormalBehavior(game));
	}
	
	public void endLoop(Ratscrew game) {
		setBehavior(behaviorFactory.getEndLoopBehavior(game));
	}
	
	public void update(Ratscrew game, int delta) {
		
	}
	
	public void makeTurn(Ratscrew game) {
		behavior.execute(game, this);
	}
}
