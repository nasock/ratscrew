package ratscrew;

public class SlapEvent extends Event {

	public SlapEvent(Player p, long l) {
		super(p, l);
	}

	@Override
	public void accept(Ratscrew game) {
		game.onEvent(this);
	}
}
