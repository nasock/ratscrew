package ratscrew;

public class BehaviorFactory {

	class Behavior {
		protected BehaviorFactory behaviorFactory;

		public Behavior(BehaviorFactory bF) {
			behaviorFactory = bF;
		}

		public void execute(Ratscrew game, Player player) {
		}

		public void init(Ratscrew game, Card card) {
		}
	}

	class NormalBehavior extends Behavior {
		public NormalBehavior(BehaviorFactory bF) {
			super(bF);
		}

		@Override
		public void execute(Ratscrew game, Player player) {
			Card topCard = game.getTopCard();
			
			if (topCard == null || !topCard.isFace()) {
				if (!player.isEnabled()) {
					return;
				}
				game.addEvent(new PutCardEvent(player));
				player.setBehavior(behaviorFactory.getEndTurnBehavior(game));
			} else {
				player.setBehavior(behaviorFactory.getLoopBehavior(game));
			}
		}
	}

	class LoopBehavior extends Behavior {
		private int currTry;
		private int tries;

		public LoopBehavior(BehaviorFactory bF) {
			super(bF);
		}

		@Override
		public void init(Ratscrew game, Card card) {
			currTry = 0;
			tries = game.countTries(card);
		}

		@Override
		public void execute(Ratscrew game, Player player) {
			if (player.isEmpty()) {
				player.setBehavior(behaviorFactory.getEndLoopBehavior(game));
				return;
			}

			if (!player.isEnabled()) {
				return;
			}

			game.addEvent(new PutCardLoopEvent(player));
			currTry++;
			if (currTry >= tries) {
				player.endLoop(game);
			}
		}
	}

	class EndLoopBehavior extends Behavior {
		public EndLoopBehavior(BehaviorFactory bF) {
			super(bF);
		}

		@Override
		public void execute(Ratscrew game, Player player) {
			Card topCard = game.getTopCard();
			if (topCard != null && !topCard.isFace()) {
				game.addEvent(new GrabDeckEvent(game.getPreviousPlayer()));
			}
			player.setBehavior(behaviorFactory.getEndTurnBehavior(game));
		}
	}

	class EndTurnBehavior extends Behavior {
		public EndTurnBehavior(BehaviorFactory bF) {
			super(bF);
		}

		@Override
		public void execute(Ratscrew game, Player player) {
			game.addEvent(new EndTurnEvent(player));
			player.setBehavior(behaviorFactory.getNormalBehavior(game));
		}
	}

	private Behavior normalBehavior;
	private Behavior loopBehavior;
	private Behavior endLoopBehavior;
	private Behavior endTurnBehavior;

	public BehaviorFactory() {
		normalBehavior = new NormalBehavior(this);
		loopBehavior = new LoopBehavior(this);
		endLoopBehavior = new EndLoopBehavior(this);
		endTurnBehavior = new EndTurnBehavior(this);
	}

	public Behavior getNormalBehavior(Ratscrew game) {
		return normalBehavior;
	}

	public Behavior getLoopBehavior(Ratscrew game) {
		Card card = game.getTopCard();
		loopBehavior.init(game, card);
		return loopBehavior;
	}

	public Behavior getEndLoopBehavior(Ratscrew game) {
		return endLoopBehavior;
	}

	public Behavior getEndTurnBehavior(Ratscrew game) {
		return endTurnBehavior;
	}

}
