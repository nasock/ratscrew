package ratscrew;

import java.util.ArrayList;

public class Settings {
	private ArrayList<PlayerInfo> players;
	private int countActivePlayers;
	private int speed;
	final private int SLOW_SPEED = 2000;
	final private int NORMAL_SPEED = 1000;
	final private int FAST_SPEED = 700;
	private Difficulty difficulty;
	private Difficulty difficultyEasy;
	private Difficulty difficultyMedium;
	private Difficulty difficultyHard;

	public Settings() {
		players = new ArrayList<PlayerInfo>();
		countActivePlayers = 4;
		speed = NORMAL_SPEED;

		difficultyEasy = new Difficulty(0.3, 1.0);
		difficultyMedium = new Difficulty(0.6, 0.8);
		difficultyHard = new Difficulty(0.9, 0.5);

		difficulty = difficultyMedium;
	}

	private void addPlayer(String name, boolean human) {
		PlayerInfo plInfo = new PlayerInfo(name, human);
		players.add(plInfo);
	}

	public void addAIPlayer(String name) {
		addPlayer(name, false);
	}

	public void addHumanPlayer(String name) {
		addPlayer(name, true);
	}

	public void addPlayer(PlayerInfo playerInfo) {
		players.add(playerInfo);
	}

	public int getActivePlayerCount() {
		return countActivePlayers;
	}

	public void setActivePlayerCount(int count) {
		countActivePlayers = count;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSlowSpeed() {
		speed = SLOW_SPEED;
	}

	public void setNormalSpeed() {
		speed = NORMAL_SPEED;
	}

	public void setFastSpeed() {
		speed = FAST_SPEED;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setEasyDifficulty() {
		difficulty = difficultyEasy;
	}

	public void setMediumDifficulty() {
		difficulty = difficultyMedium;
	}

	public void setHardDifficulty() {
		difficulty = difficultyHard;
	}

	public double getChance() {
		return difficulty.getChance();
	}

	public long getSlapTime() {
		double coeff = difficulty.getSlapCoeff();
		return (long) (speed * coeff);
	}

	public PlayerInfo getPlayerInfo(int ind) {
		return players.get(ind);
	}

	public void clean() {
		players.clear();
	}

	public Settings copySettings() {
		Settings newSet = new Settings();
		for (PlayerInfo plInf : players) {
			newSet.addPlayer(plInf);
		}
		return newSet;
	}
}
