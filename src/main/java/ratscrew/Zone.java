package ratscrew;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;

public class Zone extends JPanel {
	private Hand hand;
	
	public Zone(Color c) {
		Dimension d = new Dimension(100, 100);
		setMaximumSize(d);
		setPreferredSize(d);
		setSize(d);

		setBackground(c);
		hand = null;
	}
	
	public void setHand(Hand h) {
		hand = h;
		h.setOpaque(false);
		add(h);
	}
	
	public Hand getHand() {
		return hand;
	}
}