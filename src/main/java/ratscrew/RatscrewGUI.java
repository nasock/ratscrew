package ratscrew;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;

public class RatscrewGUI extends JFrame implements ActionListener, GameListener {
	private Settings settings;
	final private int WIDTH = 800;
	final private int HEIGHT = 720;
	final private String PATH = "data/cards/";
	private ImageLib imageLib;
	private Timer timer;
	private Logger log;
	private LogWindow logWindow;

	private ArrayList<String> menuItems;
	private final String MENU_TITLE = "Menu";
	private final String MENU_NEW = "New Game";
	private final String MENU_SHOW_LOG = "Show Log";
	private final String MENU_EXIT = "Exit";

	private ArrayList<String> playersItems;
	private final String PLAYERS_MENU_TITLE = "Players";
	private final String PLAYERS_MENU_TWO = "2 players";
	private final String PLAYERS_MENU_THREE = "3 players";
	private final String PLAYERS_MENU_FOUR = "4 players";

	private ArrayList<String> speedItems;
	private final String SPEED_MENU_TITLE = "Speed";
	private final String SPEED_MENU_SLOW = "Slow";
	private final String SPEED_MENU_NORMAL = "Normal";
	private final String SPEED_MENU_FAST = "Fast";

	private ArrayList<String> difficultyItems;
	private final String DIFFICULTY_MENU_TITLE = "Difficulty";
	private final String DIFFICULTY_MENU_EASY = "Easy";
	private final String DIFFICULTY_MENU_MEDIUM = "Medium";
	private final String DIFFICULTY_MENU_HARD = "Hard";

	final private String SLAP_STR = "slap";
	final private String NO_CARDS_STR = "no cards";

	final private int GRID_SIDE = 3;
	private Zone[] zones;
	private Ratscrew game;
	final private int BOTTOM_ZONE = 7;
	final private int TOP_ZONE = 1;
	final private int LEFT_ZONE = 5;
	final private int RIGHT_ZONE = 3;
	final private int CENTER_ZONE = 4;
	final private double SCALE = 0.6;
	private HashMap<Integer, Zone> playerToZone;
	final private Color TABLE_COLOR = new Color(24, 129, 54);
	final private Color TOOLBAR_COLOR = new Color(24, 129, 54);
	final private Color PLAYER_COLOR = new Color(248, 222, 126);
	final private Color ACTIVE_PLAYER_COLOR = new Color(255, 180, 80);
	final private Color EMPTY_ZONE_COLOR = new Color(84, 189, 114);
	final private Color WINNER_COLOR = new Color(224, 159, 114);
	private boolean paused = false;

	public RatscrewGUI(String name, Settings st) throws IOException {
		super(name);
		settings = st;
		setBounds(50, 10, WIDTH, HEIGHT);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		imageLib = new ImageLib(PATH);
		timer = new Timer(settings.getSpeed(), this);
		log = new Logger();
		logWindow = new LogWindow(RatscrewGUI.this, log);

		menuItems = new ArrayList<String>();
		menuItems.add(MENU_NEW);
		menuItems.add(MENU_SHOW_LOG);
		menuItems.add(MENU_EXIT);
		JMenu fileMenu = makeMenu(menuItems, MENU_TITLE);

		playersItems = new ArrayList<String>();
		playersItems.add(PLAYERS_MENU_TWO);
		playersItems.add(PLAYERS_MENU_THREE);
		playersItems.add(PLAYERS_MENU_FOUR);
		JMenu playersMenu = makeMenu(playersItems, PLAYERS_MENU_TITLE);

		speedItems = new ArrayList<String>();
		speedItems.add(SPEED_MENU_SLOW);
		speedItems.add(SPEED_MENU_NORMAL);
		speedItems.add(SPEED_MENU_FAST);
		JMenu speedMenu = makeMenu(speedItems, SPEED_MENU_TITLE);

		difficultyItems = new ArrayList<String>();
		difficultyItems.add(DIFFICULTY_MENU_EASY);
		difficultyItems.add(DIFFICULTY_MENU_MEDIUM);
		difficultyItems.add(DIFFICULTY_MENU_HARD);
		JMenu difficultyMenu = makeMenu(difficultyItems, DIFFICULTY_MENU_TITLE);

		JMenuBar menu = new JMenuBar();
		menu.add(fileMenu);
		menu.add(playersMenu);
		menu.add(speedMenu);
		menu.add(difficultyMenu);
		setJMenuBar(menu);

		addAllPanels();
	}

	private JMenu makeMenu(ArrayList<String> list, String menuName) {
		JMenu fileMenu = new JMenu(menuName);
		for (String menuItem : list) {
			JMenuItem item = new JMenuItem(menuItem);
			item.setActionCommand(menuItem);
			item.addActionListener(new MenuListener());
			fileMenu.add(item);
		}
		return fileMenu;
	}

	private class MenuListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();
			if (MENU_NEW.equals(command)) {
				restart();
			} else if (MENU_SHOW_LOG.equals(command)) {
				showLog();
			} else if (MENU_EXIT.equals(command)) {
				System.exit(0);
			} else if (PLAYERS_MENU_TWO.equals(command)) {
				restart(2);
			} else if (PLAYERS_MENU_THREE.equals(command)) {
				restart(3);
			} else if (PLAYERS_MENU_FOUR.equals(command)) {
				restart(4);
			} else if (SPEED_MENU_SLOW.equals(command)) {
				settings.setSlowSpeed();
				updateTimerDelay();
			} else if (SPEED_MENU_NORMAL.equals(command)) {
				settings.setNormalSpeed();
				updateTimerDelay();
			} else if (SPEED_MENU_FAST.equals(command)) {
				settings.setFastSpeed();
				updateTimerDelay();
			} else if (DIFFICULTY_MENU_EASY.equals(command)) {
				settings.setEasyDifficulty();
			} else if (DIFFICULTY_MENU_MEDIUM.equals(command)) {
				settings.setMediumDifficulty();
			} else if (DIFFICULTY_MENU_HARD.equals(command)) {
				settings.setHardDifficulty();
			}
		}
	}

	private void addAllPanels() {
		Container container = getContentPane();

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.setBackground(TOOLBAR_COLOR);

		JPanel innerPanel = new JPanel();
		innerPanel.setLayout(new GridLayout(GRID_SIDE, GRID_SIDE));
		innerPanel.setBackground(EMPTY_ZONE_COLOR);

		createToolbar(buttonPanel);
		createZones(innerPanel);

		panel.add(BorderLayout.NORTH, buttonPanel);
		panel.add(BorderLayout.CENTER, innerPanel);
		container.add(panel);
		pack();
	}

	private void drawButton(JButton button, Image img) {
		button.setMargin(new Insets(0, 0, 0, 0));
		button.setBackground(TOOLBAR_COLOR);
		button.setBorder(null);
		button.setIcon(new ImageIcon(img));
		button.setEnabled(true);
	}

	private void createToolbar(JPanel panel) {
		JButton restartButton = new JButton();
		Image restartIcon = imageLib.getRestartIcon();
		drawButton(restartButton, restartIcon);

		final JButton pauseButton = new JButton();
		Image pIcon;
		if (paused) {
			pIcon = imageLib.getPlayIcon();
		} else {
			pIcon = imageLib.getPauseIcon();
		}
		drawButton(pauseButton, pIcon);

		JButton logtButton = new JButton();
		Image logIcon = imageLib.getLogIcon();
		drawButton(logtButton, logIcon);

		restartButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				restart();
			}
		});

		pauseButton.addMouseListener(new MouseAdapter() {
			Image pauseIcon = imageLib.getPauseIcon();
			Image playIcon = imageLib.getPlayIcon();
			
			public void mouseClicked(MouseEvent e) {
				if (paused) {
					paused = false;
					pauseButton.setIcon(new ImageIcon(pauseIcon));
					timer.start();
				} else {
					paused = true;
					pauseButton.setIcon(new ImageIcon(playIcon));
					timer.stop();
				}
			}
		});

		logtButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				showLog();
			}
		});

		panel.add(restartButton);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
		panel.add(pauseButton);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
		panel.add(logtButton);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
	}

	private void changeSettings(int count) {
		settings.setActivePlayerCount(count);
	}

	private void showLog() {
		logWindow.toggleLog();
	}

	private void restart() {
		int count = settings.getActivePlayerCount();
		restart(count);
	}

	private void restart(int countPlayers) {
		timer.stop();
		Container container = getContentPane();
		container.removeAll();
		changeSettings(countPlayers);
		addAllPanels();
		start();
	}

	private void updateTimerDelay() {
		game.onChangeSettings(settings);
		timer.setDelay(settings.getSpeed());
	}

	private void createZones(JPanel panel) {
		zones = new Zone[] { new Zone(EMPTY_ZONE_COLOR), new Zone(PLAYER_COLOR), new Zone(EMPTY_ZONE_COLOR),
				new Zone(PLAYER_COLOR), new Zone(TABLE_COLOR), new Zone(PLAYER_COLOR), new Zone(EMPTY_ZONE_COLOR),
				new Zone(PLAYER_COLOR), new Zone(EMPTY_ZONE_COLOR) };

		for (Zone z : zones) {
			panel.add(z);
			z.setLayout(new BoxLayout(z, BoxLayout.PAGE_AXIS));
			z.setBorder(BorderFactory.createEmptyBorder(15, 0, 0, 0));
		}

		playerToZone = new HashMap<Integer, Zone>();
		int count = settings.getActivePlayerCount();
		putPlayerAndZoneToMap(count);
	}

	private void putPlayerAndZoneToMap(int count) {
		if (count == 2) {
			playerToZone.put(0, getZone(BOTTOM_ZONE));
			playerToZone.put(1, getZone(TOP_ZONE));
		} else if (count == 3) {
			playerToZone.put(0, getZone(BOTTOM_ZONE));
			playerToZone.put(1, getZone(RIGHT_ZONE));
			playerToZone.put(2, getZone(TOP_ZONE));
		} else if (count == 4) {
			playerToZone.put(0, getZone(BOTTOM_ZONE));
			playerToZone.put(1, getZone(RIGHT_ZONE));
			playerToZone.put(2, getZone(TOP_ZONE));
			playerToZone.put(3, getZone(LEFT_ZONE));
		}
	}

	private Zone getZone(int i) {
		return zones[i];
	}

	private Hand addHandToZone(Zone z, Image im, String str) {
		Hand h = new Hand(im, SCALE, str);
		z.setHand(h);
		return h;
	}

	private void addMouseListenerToDeck(Hand hand, final HumanPlayer player) {
		hand.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (paused == true) {
					return;
				}
				player.enable();
			}
		});
	}

	private void addSlapButton(Hand hand, final HumanPlayer player) {
		JButton button = hand.addButton(SLAP_STR);
		button.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (paused == true) {
					return;
				}
				game.addEvent(new SlapEvent(player, 0));
			}
		});
	}

	private void start() {
		game = new Ratscrew();
		game.addListener(this);
		game.addListener(log);
		game.start(settings);
		log.clear();

		for (int i = 0; i < settings.getActivePlayerCount(); i++) {
			Player player = game.getPlayer(i);
			Zone zone = playerToZone.get(i);
			Image im = imageLib.getBack();
			String str = player.toString();

			Hand hand = addHandToZone(zone, im, str);

			if (i == 0) {
				zone.setBackground(ACTIVE_PLAYER_COLOR);
			} else {
				zone.setBackground(PLAYER_COLOR);
			}

			if (player.isHuman()) {
				HumanPlayer humanPlayer = (HumanPlayer) player;
				addMouseListenerToDeck(hand, humanPlayer);
				addSlapButton(hand, humanPlayer);
			}
		}

		Image im = imageLib.getNoCards();
		addHandToZone(getZone(CENTER_ZONE), im, NO_CARDS_STR);

		validate();

		timer.start();
	}

	private Zone getPlayerZone(Player p) {
		int id = p.getId();
		return playerToZone.get(id);
	}

	private Hand getCenterHand() {
		Zone centerZone = getZone(CENTER_ZONE);
		Hand centerHand = centerZone.getHand();
		return centerHand;
	}

	private Hand getPlayerHand(Player p) {
		int id = p.getId();
		Zone zone = playerToZone.get(id);
		return zone.getHand();
	}

	private void setImageAndTextToPlayer(Player player) {
		Hand playerHand = getPlayerHand(player);
		Image im;
		if (player.isEmpty()) {
			im = imageLib.getNoCards();
		} else {
			im = imageLib.getBack();
		}
		playerHand.setImage(im);
		String str = player.toString();
		playerHand.setText(str);
	}

	private void setTextToCenter(String str) {
		Hand centerHand = getCenterHand();
		centerHand.setText(str);
	}

	private void setImageToCenter(Card card) {
		Hand centerHand = getCenterHand();
		Image im;
		if (card == null) {
			im = imageLib.getNoCards();
		} else {
			im = imageLib.getImage(card);
		}
		centerHand.setImage(im);
	}

	public void actionPerformed(ActionEvent e) {
		game.update(settings.getSpeed());
	}

	public void onPutCard(Ratscrew game, Player player, Card card) {
		setImageAndTextToPlayer(player);
		setImageToCenter(card);
		int deckSize = game.getGameDeckSize();
		setTextToCenter("" + deckSize);
	}

	public void onPlayerDeckGrab(Ratscrew game, Player player) {
		setImageAndTextToPlayer(player);
		setImageToCenter(null);
		setTextToCenter(NO_CARDS_STR);
	}

	public void onPlayerHasNoCards(Ratscrew game, Player player) {
	}

	public void onChangePlayer(Ratscrew game, Player player) {
		for (int i = 0; i < playerToZone.size(); i++) {
			Zone zone = playerToZone.get(i);
			zone.setBackground(PLAYER_COLOR);
		}

		Zone playerZone = getPlayerZone(player);
		playerZone.setBackground(ACTIVE_PLAYER_COLOR);
	}

	public void onComplete(Ratscrew game, Player player) {
		timer.stop();
		for (int i = 0; i < game.getPlayersSize(); i++) {
			Player p = game.getPlayer(i);
			setImageAndTextToPlayer(p);
		}

		Zone playerZone = getPlayerZone(player);
		playerZone.setBackground(WINNER_COLOR);

		setTextToCenter(player.toString() + " IS WINNER");
	}

	public void onSlap(Ratscrew game, Player player, boolean success) {
		setImageAndTextToPlayer(player);
		if (success) {
			setTextToCenter(player.toString() + " SLAPPED");
		} else {
			setTextToCenter(player.toString() + " PUT 2 CARDS");
			Card card = game.getTopCard();
			setImageToCenter(card);
		}
	}

	private static Settings createSettings(String[] names, Double[] chances) {
		Settings set = new Settings();
		for (int i = 0; i < names.length; i++) {
			String name = names[i];
			double chance = chances[i];
			if (chance == 0.0) {
				set.addHumanPlayer(name);
			} else {
				set.addAIPlayer(name);
			}
		}
		return set;
	}

	public static void main(String[] args) throws IOException {
		String[] names = new String[] { "D", "Gapochka", "Mihail Krug", "Cartoshka" };
		Double[] chances = new Double[] { 0.0, 1.0, 1.0, 1.0 };
		Settings st = createSettings(names, chances);

		RatscrewGUI rs = new RatscrewGUI("RATSCREW", st);
		rs.start();
		rs.setVisible(true);
	}
}
