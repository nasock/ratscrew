package ratscrew;

public class MarigeCombination extends Combination {

	@Override
	public boolean isPresent(Deck d) {
		if(d.getSize() < 2) {
			return false;
		}
		Card firstCard = d.getFirstCard();
		Card secondCard = d.getSecondCard();
		CardValue firstV = firstCard.getValue();
		CardValue secondV = secondCard.getValue();
		if ((firstV.equals(CardValue.QUEEN) && secondV.equals(CardValue.KING))
				|| (firstV.equals(CardValue.KING) && secondV.equals(CardValue.QUEEN))) {
			return true;
		}
		return false;
	}

}
