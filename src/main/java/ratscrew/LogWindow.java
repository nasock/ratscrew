package ratscrew;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

public class LogWindow extends JDialog implements Logger.LoggerListener{
	private JTextArea text;
	private Logger log;
	private final String TITLE_STR = "Game Events";
	private int counter;
	
	public LogWindow(JFrame owner, Logger l) {
		super(owner);
		createGUI();
		log = l;
		log.addListener(this);
	}

	private void createGUI() {
		setPreferredSize(new Dimension(400, 700));
		setTitle(TITLE_STR);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		
		Container container = getContentPane();
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		text = new JTextArea();
		text.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(text);
		scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
		scrollPane.setBackground(new Color(255, 255, 255));
		
		panel.add(scrollPane);
		container.add(panel);
		pack();
		setLocationRelativeTo(getParent());
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	public void toggleLog() {
		setVisible(!isVisible());
	}

	public void onLog(String message) {
		counter++;
		text.append(counter + ") " + message+ "\n");
	}

	public void onClear() {
		counter = 0;
		text.setText("");
	}
}
