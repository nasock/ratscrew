package ratscrew;

public class PutCardLoopEvent extends Event {

	public PutCardLoopEvent(Player p) {
		super(p);
	}

	@Override
	public void accept(Ratscrew game) {
		game.onEvent(this);
	}

}
