package ratscrew;

public class DoubleCombination extends Combination{

	@Override
	public boolean isPresent(Deck d) {
		if(d.getSize() < 2) {
			return false;
		}
		
		Card firstCard = d.getFirstCard();
		Card secondCard = d.getSecondCard();
		if(firstCard.getValue().equals(secondCard.getValue())) {
			return true;
		}
		return false;
	}

}
