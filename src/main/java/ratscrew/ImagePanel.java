package ratscrew;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {

	private BufferedImage image;
	private Image scaled;

	public ImagePanel(String path) {

		try {
			image = ImageIO.read(new File(path));
			int w = image.getWidth();
			int h = image.getHeight();
			Dimension d = new Dimension(w, h);
			setMaximumSize(d);
			setPreferredSize(d);
			setSize(d);
		} catch (IOException ex) {
			System.out.println("Exception");
		}
	}

	public void scale(double s) {
		int w = (int) (image.getWidth() * s);
		int h = (int) (image.getHeight() * s);
		Dimension d = new Dimension(w, h);
		setMaximumSize(d);
		setPreferredSize(d);
		setSize(d);
		scaled = image.getScaledInstance(w, h, Image.SCALE_SMOOTH);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (scaled == null) {
			g.drawImage(image, 0, 0, this);
		} else {
			g.drawImage(scaled, 0, 0, this);
		}
	}

}