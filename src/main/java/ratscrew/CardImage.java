package ratscrew;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

public class CardImage extends JPanel {
	private Image scaled;
	private double scaleRatio;
	
	public CardImage(Image image, double s) {
		scaleRatio = s;
		scale(image);
	}

	public void scale(Image image) {
		int w = (int) (image.getWidth(null) * scaleRatio);
		int h = (int) (image.getHeight(null) * scaleRatio);
		Dimension d = new Dimension(w, h);
		setMaximumSize(d);
		setPreferredSize(d);
		setSize(d);
		scaled = image.getScaledInstance(w, h, Image.SCALE_SMOOTH);
	}
	
	public void setImage(Image image) {
		scale(image);
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(scaled, 0, 0, this);
	}

}
