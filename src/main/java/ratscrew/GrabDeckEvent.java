package ratscrew;

public class GrabDeckEvent extends Event {

	public GrabDeckEvent(Player p) {
		super(p);
	}

	@Override
	public void accept(Ratscrew game) {
		game.onEvent(this);
	}

}
