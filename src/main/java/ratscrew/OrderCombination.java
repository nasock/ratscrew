package ratscrew;

public class OrderCombination extends Combination {

	@Override
	public boolean isPresent(Deck d) {
		if(d.getSize() < 3) {
			return false;
		}
		
		int count = 0;
		for (int i = 0; i < d.getSize() - 3; i++) {
			Card firstC = d.getFromTop(i);
			Card secondC = d.getFromTop(i+1);
			Card thirdC = d.getFromTop(i+2);
			int firstDiff = firstC.getValueDifference(secondC);
			int secondDiff = secondC.getValueDifference(thirdC);
			if ((firstDiff == secondDiff) && Math.abs(secondDiff) == 1) {
				count++;
			} else {
				break;
			}
		}
		
		if(count < 1) {
			return false;
		}
		return true;
	}

}
