package ratscrew;

public class GameOutput implements GameListener {

	public void onPutCard(Ratscrew g, Player p, Card c) {
		System.out.println(p + " put card " + c);
	}

	public void onPlayerDeckGrab(Ratscrew g, Player p) {
		System.out.println(p + " grab the deck");
	}

	public void onPlayerHasNoCards(Ratscrew g, Player p) {
		// System.out.println(p + " has no cards");
	}

	public void onChangePlayer(Ratscrew g, Player p) {
		// System.out.println(p + " turn");
	}

	public void onComplete(Ratscrew g, Player p) {
		System.out.println("game is over. " + p + " won");
	}

	public void onSlap(Ratscrew g, Player p, boolean success) {
		String s;
		if (success) {
			s = "successfully";
		} else {
			s = "unsuccessfully";
		}
		System.out.println(p + " slapped " + s);
	}

}
