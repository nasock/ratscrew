package ratscrew;

public class PutCardEvent extends Event {

	public PutCardEvent(Player p) {
		super(p);
	}

	@Override
	public void accept(Ratscrew game) {
		game.onEvent(this);
	}
}
