package ratscrew;

public class EndTurnEvent extends Event {

	public EndTurnEvent(Player p) {
		super(p);
	}

	@Override
	public void accept(Ratscrew game) {
		game.onEvent(this);
	}
}
