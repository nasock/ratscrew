package ratscrew;

public class Card extends Object{
	private CardValue value;
	private CardSuit suit;

	public Card(CardValue v, CardSuit s) {
		value = v;
		suit = s;
	}
	

	public CardValue getValue() {
		return value;
	}

	public CardSuit getSuit() {
		return suit;
	}

	public boolean greaterThan(Card c) {
		CardValue otherValue = c.getValue();
		return value.getNumValue() > otherValue.getNumValue();
	}
	
	public boolean isSameValue(Card c) {
		CardValue otherValue = c.getValue();
		return value == otherValue;
	}
	
	public int getValueDifference(Card c) {
		CardValue otherValue = c.getValue();
		return value.getNumValue() - otherValue.getNumValue();
	}

	public boolean isSameSuit(Card c) {
		CardSuit otherSuit = c.getSuit();
		return suit == otherSuit;
	}
	
	public boolean isFace() {
		return CardValue.isFace(this);
	}
	
	@Override
	public int hashCode() {
		int s = suit.ordinal()*100;
		int v = value.ordinal();
		return s + v;
	}
	
	@Override
	public boolean equals(Object o) {
		Card c = (Card) o;
		return isSameSuit(c) && isSameValue(c);
	}

	public String toString() {
		return value.toString() + " of " + suit.toString();
	}

//	public static void main(String[] args) {
//		Card c0 = new Card(CardValue.TWO, CardSuit.DIAMONDS);
//		Card c1 = new Card(CardValue.SEVEN, CardSuit.SPADES);
//		Card c2 = new Card(CardValue.SEVEN, CardSuit.SPADES);
//
//		System.out.println(c1.getValueDifference(c0));
//	}
}
