package ratscrew;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class ImageLib {
	private HashMap<Card, Image> cardsToImages;
	private Image imageBack;
	private Image imageNoCards;
	private Image iconRestart;
	private Image iconPause;
	private Image iconPlay;
	private Image iconLogNotActive;
	private Image iconLogActive;

	public ImageLib(String path) throws IOException {
		cardsToImages = new HashMap<Card, Image>();

		putToMap(CardValue.ACE, CardSuit.CLUBS, path + "club_1.png");
		putToMap(CardValue.TWO, CardSuit.CLUBS, path + "club_2.png");
		putToMap(CardValue.THREE, CardSuit.CLUBS, path + "club_3.png");
		putToMap(CardValue.FOUR, CardSuit.CLUBS, path + "club_4.png");
		putToMap(CardValue.FIVE, CardSuit.CLUBS, path + "club_5.png");
		putToMap(CardValue.SIX, CardSuit.CLUBS, path + "club_6.png");
		putToMap(CardValue.SEVEN, CardSuit.CLUBS, path + "club_7.png");
		putToMap(CardValue.EIGHT, CardSuit.CLUBS, path + "club_8.png");
		putToMap(CardValue.NINE, CardSuit.CLUBS, path + "club_9.png");
		putToMap(CardValue.TEN, CardSuit.CLUBS, path + "club_10.png");
		putToMap(CardValue.JACK, CardSuit.CLUBS, path + "club_jack.png");
		putToMap(CardValue.KING, CardSuit.CLUBS, path + "club_king.png");
		putToMap(CardValue.QUEEN, CardSuit.CLUBS, path + "club_queen.png");

		putToMap(CardValue.ACE, CardSuit.DIAMONDS, path + "diamond_1.png");
		putToMap(CardValue.TWO, CardSuit.DIAMONDS, path + "diamond_2.png");
		putToMap(CardValue.THREE, CardSuit.DIAMONDS, path + "diamond_3.png");
		putToMap(CardValue.FOUR, CardSuit.DIAMONDS, path + "diamond_4.png");
		putToMap(CardValue.FIVE, CardSuit.DIAMONDS, path + "diamond_5.png");
		putToMap(CardValue.SIX, CardSuit.DIAMONDS, path + "diamond_6.png");
		putToMap(CardValue.SEVEN, CardSuit.DIAMONDS, path + "diamond_7.png");
		putToMap(CardValue.EIGHT, CardSuit.DIAMONDS, path + "diamond_8.png");
		putToMap(CardValue.NINE, CardSuit.DIAMONDS, path + "diamond_9.png");
		putToMap(CardValue.TEN, CardSuit.DIAMONDS, path + "diamond_10.png");
		putToMap(CardValue.JACK, CardSuit.DIAMONDS, path + "diamond_jack.png");
		putToMap(CardValue.KING, CardSuit.DIAMONDS, path + "diamond_king.png");
		putToMap(CardValue.QUEEN, CardSuit.DIAMONDS, path + "diamond_queen.png");

		putToMap(CardValue.ACE, CardSuit.HEARTS, path + "heart_1.png");
		putToMap(CardValue.TWO, CardSuit.HEARTS, path + "heart_2.png");
		putToMap(CardValue.THREE, CardSuit.HEARTS, path + "heart_3.png");
		putToMap(CardValue.FOUR, CardSuit.HEARTS, path + "heart_4.png");
		putToMap(CardValue.FIVE, CardSuit.HEARTS, path + "heart_5.png");
		putToMap(CardValue.SIX, CardSuit.HEARTS, path + "heart_6.png");
		putToMap(CardValue.SEVEN, CardSuit.HEARTS, path + "heart_7.png");
		putToMap(CardValue.EIGHT, CardSuit.HEARTS, path + "heart_8.png");
		putToMap(CardValue.NINE, CardSuit.HEARTS, path + "heart_9.png");
		putToMap(CardValue.TEN, CardSuit.HEARTS, path + "heart_10.png");
		putToMap(CardValue.JACK, CardSuit.HEARTS, path + "heart_jack.png");
		putToMap(CardValue.KING, CardSuit.HEARTS, path + "heart_king.png");
		putToMap(CardValue.QUEEN, CardSuit.HEARTS, path + "heart_queen.png");

		putToMap(CardValue.ACE, CardSuit.SPADES, path + "spade_1.png");
		putToMap(CardValue.TWO, CardSuit.SPADES, path + "spade_2.png");
		putToMap(CardValue.THREE, CardSuit.SPADES, path + "spade_3.png");
		putToMap(CardValue.FOUR, CardSuit.SPADES, path + "spade_4.png");
		putToMap(CardValue.FIVE, CardSuit.SPADES, path + "spade_5.png");
		putToMap(CardValue.SIX, CardSuit.SPADES, path + "spade_6.png");
		putToMap(CardValue.SEVEN, CardSuit.SPADES, path + "spade_7.png");
		putToMap(CardValue.EIGHT, CardSuit.SPADES, path + "spade_8.png");
		putToMap(CardValue.NINE, CardSuit.SPADES, path + "spade_9.png");
		putToMap(CardValue.TEN, CardSuit.SPADES, path + "spade_10.png");
		putToMap(CardValue.JACK, CardSuit.SPADES, path + "spade_jack.png");
		putToMap(CardValue.KING, CardSuit.SPADES, path + "spade_king.png");
		putToMap(CardValue.QUEEN, CardSuit.SPADES, path + "spade_queen.png");

		loadBack();
		loadNoCards();
		loadRestartIcon();
		loadPauseIcon();
		loadPlayIcon();
		loadnLogNotActiveIcon();
		loadnLogActiveIcon();
	}

	private void putToMap(CardValue v, CardSuit s, String path) throws IOException {
		Card c = new Card(v, s);
		Image im = loadImage(path);
		cardsToImages.put(c, im);
	}

	public Image loadImage(final String pathAndFileName) throws IOException{
		final URL url = Thread.currentThread().getContextClassLoader().getResource(pathAndFileName);
		if (url == null) {
			throw new IOException();
		}
		return ImageIO.read(url);
	}

	public Image getImage(Card c) {
		return cardsToImages.get(c);
	}

	private void loadBack() throws IOException {
		try {
			imageBack = loadImage("data/cards/back-green.png");
		} catch (IOException ex) {
			System.out.println("Exception");
		}
	}

	public Image getBack() {
		return imageBack;
	}

	private void loadNoCards() throws IOException {
		try {
			imageNoCards = loadImage("data/cards/card-base.png");
		} catch (IOException ex) {
			System.out.println("Exception");
		}
	}

	public Image getNoCards() {
		return imageNoCards;
	}

	private void loadRestartIcon() throws IOException {
		try {
			iconRestart = loadImage("data/icons/restart.png");
		} catch (IOException ex) {
			System.out.println("Exception");
		}
	}

	public Image getRestartIcon() {
		return iconRestart;
	}

	private void loadPauseIcon() throws IOException {
		try {
			iconPause = loadImage("data/icons/pause.png");
		} catch (IOException ex) {
			System.out.println("Exception");
		}
	}

	public Image getPauseIcon() {
		return iconPause;
	}

	private void loadPlayIcon() throws IOException {
		try {
			iconPlay = loadImage("data/icons/play.png");
		} catch (IOException ex) {
			System.out.println("Exception");
		}
	}

	public Image getPlayIcon() {
		return iconPlay;
	}

	private void loadnLogNotActiveIcon() throws IOException {
		try {
			iconLogNotActive = loadImage("data/icons/information-outline.png");
		} catch (IOException ex) {
			System.out.println("Exception");
		}
	}

	public Image getLogIcon() {
		return iconLogNotActive;
	}

	private void loadnLogActiveIcon() throws IOException {
		try {
			iconLogActive = loadImage("data/icons/information.png");
		} catch (IOException ex) {
			System.out.println("Exception");
		}
	}

	public Image getLogActiveButton() {
		return iconLogActive;
	}

}
